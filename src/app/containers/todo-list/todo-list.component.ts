import { Component, OnDestroy } from '@angular/core';
import { Task } from 'src/app/models/task';
import { TodoService } from '../../services/todo.service';
import { BehaviorSubject } from 'rxjs';

@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.scss'],
})
export class TodoListComponent implements OnDestroy {
  task: Task = { name: '', done: false };
  rows$: BehaviorSubject<Task[]>;
  isEdition = false;

  constructor(private todoService: TodoService) {
    this.todoService.getTasks();
    this.rows$ = this.todoService.tasks$;
  }

  ngOnDestroy(): void {
    this.rows$.unsubscribe();
  }

  onSubmit(task: Task): void {
    const taskCopy = { ...task };
    task.name = '';
    if (taskCopy.id) {
      this.todoService.updateTask(taskCopy).subscribe((resp: Task) => {
        const newList = this.rows$.value.map((row) =>
          row.id === resp.id ? { ...resp } : row
        );
        this.isEdition = false;
        this.rows$.next(newList);
      });
    } else {
      this.todoService.createTask(taskCopy).subscribe((res) => {
        this.rows$.next([...this.rows$.value, res]);
      });
    }
  }

  executeAction(action: any): void {
    if (action.action === 'delete') {
      this.todoService.deleteTask(action.task.id).subscribe((response) => {
        const newList = this.rows$.value.filter(
          (row) => row.id !== response.id
        );
        this.rows$.next(newList);
      });
    } else if (action.action === 'update') {
      this.isEdition = true;
      this.task = { ...action.task };
    } else if (action.action === 'update-done') {
      this.todoService
        .updateTask({ ...action.task, done: !action.task.done })
        .subscribe();
    }
  }
}

import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Task } from '../models/task';
import { BehaviorSubject, Observable, Subscription, throwError } from 'rxjs';
import { environment } from 'src/environments/environment';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class TodoService {
  url: string = `${environment.backendUrl}/todo`;
  public tasks$: BehaviorSubject<Task[]> = new BehaviorSubject<Task[]>([]);

  constructor(private http: HttpClient) {}

  createTask(task: Task): Observable<Task> {
    return this.http.post<Task>(this.url, task).pipe(
      catchError((error: HttpErrorResponse) => {
        console.error('Error creating task');
        return throwError(error.message);
      })
    );
  }
  updateTask(task: Task): Observable<Task> {
    return this.http.put<Task>(this.url + '/' + task.id, task).pipe(
      catchError((error: HttpErrorResponse) => {
        console.error('Error updating task');
        return throwError(error.message);
      })
    );
  }
  getTasks(): Subscription {
    return this.http
      .get<Task[]>(this.url)
      .pipe(
        catchError((error: HttpErrorResponse) => {
          console.error('Error getting tasks');
          return throwError(error.message);
        })
      )
      .subscribe((response) => {
        this.tasks$.next(response);
      });
  }
  deleteTask(taskId: number): Observable<Task> {
    return this.http.delete<Task>(this.url + '/' + taskId).pipe(
      catchError((error: HttpErrorResponse) => {
        console.error('Error deleting task');
        return throwError(error.message);
      })
    );
  }
}

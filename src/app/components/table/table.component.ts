import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Task } from 'src/app/models/task';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss'],
})
export class TableComponent {
  @Input() header: string[] = [];
  @Input() rows: Task[] = [];
  @Output() action = new EventEmitter();

  sendAction(action: string, task: Task): void {
    this.action.emit({ action, task });
  }
}
